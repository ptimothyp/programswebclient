import { Ng2centricPage } from './app.po'
import { browser } from 'protractor'

describe('ng2centric App', () => {
    let page: Ng2centricPage

    browser.waitForAngularEnabled(false)

    beforeEach(() => {
        page = new Ng2centricPage()
    })

    it('should perform a basic test', () => {
        page.navigateTo()
        const root = page.getRootElement()
        expect(root.count()).toEqual(1)
    })
})
