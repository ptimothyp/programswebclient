import { createSelector } from '@ngrx/store'
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity'
import { Program } from '../models'
import * as programActions from '../actions/program'

export interface State extends EntityState<Program> {
    selectedProgramId: number
    filter: string
}

const getId = (program: Program) => program.id

export const adapter: EntityAdapter<Program> = createEntityAdapter<Program>({
    selectId: getId,
    sortComparer: false
})

export const initialState: State = adapter.getInitialState({
    selectedProgramId: null,
    filter: ''
})

export function reducer(state = initialState, action: programActions.Actions): State {
    switch (action.type) {
        case programActions.LOAD_ALL_SUCCESS: {
            return adapter.addAll(action.programs, state)
        }
        case programActions.SELECT: {
            return {...state, selectedProgramId: action.programId}
        }
        case programActions.FILTER: {
            return { ...state, filter: action.filter}
        }
        default: {
            return state
        }
    }
}

export const {
    selectAll: getAllPrograms,
    selectEntities: getAllProgramEntities
} = adapter.getSelectors()

export const getSelectedProgramId = (state: State) => state.selectedProgramId
export const getSelectedProgram = createSelector(getAllProgramEntities, getSelectedProgramId, (programs, id) => programs[id])

export const getFilter = (state: State) => state.filter
export const getFilteredPrograms = createSelector(getAllPrograms, getFilter, (programs, filter) => {
    if (!filter) {
        return programs
    }
    return programs.filter(program =>
        program.name.toLowerCase().indexOf(filter.toLowerCase()) > -1
        || program.agency.toLowerCase().indexOf(filter.toLowerCase()) > -1
    )
})