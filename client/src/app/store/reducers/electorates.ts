import { createSelector } from '@ngrx/store'
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity'
import { Electorate } from '../models'
import * as electorate from '../actions/electorate'
import { Update } from '@ngrx/entity/src/models'

export interface State extends EntityState<Electorate> {
    selectedElectorateName: string,
    filter: string
}

export const adapter: EntityAdapter<Electorate> = createEntityAdapter<Electorate>({
    selectId: (e: Electorate) => e.name,
    sortComparer: (a, b) => a.name.localeCompare(b.name)
})

export const initialState: State = adapter.getInitialState({
    selectedElectorateName: '',
    filter: '',
})

export function reducer(state = initialState, action: electorate.Actions): State {
    switch (action.type) {
        case electorate.LOAD_ALL_SUCCESS: {
            return adapter.addAll(action.electorates, state)
        }
        case electorate.LOAD_STATISTICS: {
            const changes: Partial<Electorate> = { statisticIds: action.statistics.map(report => report.id) }
            const update: Update<Electorate> = { id: action.electorate, changes }
            const newState = adapter.updateOne(update, state)
            return newState
        }
        case electorate.SELECT: {
            return {
                ...state,
                selectedElectorateName: action.electorate
            }
        }
        case electorate.FILTER: {
            return {
                ...state,
                filter: action.filter
            }
        }
        case electorate.TRANSFORM_MAP_DATA_SUCCESS: {
            return adapter.updateOne(
                {
                    id: state.selectedElectorateName,
                    changes: {
                        ...state.entities[state.selectedElectorateName],
                        mapdata: action.transformedMapData
                    }
                }, state)
        }
        default: {
            return state
        }
    }
}

export const getSelectedElectorateName = (state: State) => state.selectedElectorateName
export const getFilter = (state: State) => state.filter

export const {
    selectAll: getAllElectorates,
    selectEntities: getAllElectorateEntities
} = adapter.getSelectors()

export const getSelectedElectorate = createSelector(getAllElectorateEntities, getSelectedElectorateName, (electorates, name) =>
    electorates[name]
)

export const getFilteredElectorates = createSelector(getAllElectorates, getFilter, (electorates, filter) => {
    if (!filter) {
        return electorates
    }
    return electorates.filter(e =>
        e.party.toLowerCase() === filter.toLowerCase()
        || e.state.toLowerCase() === filter.toLowerCase()
        || e.name.toLowerCase().indexOf(filter.toLowerCase()) > -1
        || e.member.toLowerCase().indexOf(filter.toLowerCase()) > -1
    )
})
