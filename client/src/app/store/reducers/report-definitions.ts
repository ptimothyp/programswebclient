import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity'
import { ReportDefinition } from '../models'
import * as reportActions from '../actions/report'

export interface State extends EntityState<ReportDefinition> {}

export const adapter: EntityAdapter<ReportDefinition> = createEntityAdapter<ReportDefinition>({
    selectId: reportDef => reportDef.id,
    sortComparer: false
})

export const initialState: State = adapter.getInitialState()

export function reducer(state = initialState, action: reportActions.Actions): State {
    switch (action.type) {
        case reportActions.LOAD_DEFINITIONS: {
            return adapter.addMany(action.definitions, state)
        }
        default: {
            return state
        }
    }
}
