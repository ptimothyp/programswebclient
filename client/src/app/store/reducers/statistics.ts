import { createSelector } from '@ngrx/store'
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity'
import { Statistic } from '../models'
import * as statistic from '../actions/statistics'
import * as electorate from '../actions/electorate'

export interface State extends EntityState<Statistic> {
    selectedStatisticId: number
}

export const adapter: EntityAdapter<Statistic> = createEntityAdapter<Statistic>({
    selectId: stat => stat.id,
    sortComparer: false
})

export const initialState: State = adapter.getInitialState({
    selectedStatisticId: -1
})

export function reducer(state = initialState, action: statistic.Actions | electorate.Actions): State {
    switch (action.type) {
        case statistic.SELECT: {
            return {
                ...state,
                selectedStatisticId: action.statisticId
            }
        }
        case electorate.LOAD_STATISTICS: {
            return adapter.addMany(action.statistics, state)
        }
        default: {
            return state
        }
    }
}

export const {
    selectEntities: getStatisticEntities,
    selectAll: getAllStatistics
} = adapter.getSelectors()

export const getSelectedStatisticId = (state: State) => state.selectedStatisticId
export const getSelectedStatistic = createSelector(getStatisticEntities, getSelectedStatisticId, (statistics, id) => statistics[id])