import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity'
import { Report } from '../models'
import * as report from '../actions/report'

export interface State extends EntityState<Report> {
    selectedReportDefId: number
}

export const adapter: EntityAdapter<Report> = createEntityAdapter<Report>({
    selectId: r => r.id,
    sortComparer: false
})

export const initialState: State = adapter.getInitialState({
    selectedReportDefId: -1
})

export function reducer(state = initialState, action: report.Actions): State {
    switch (action.type) {
        case report.LOAD_REPORTS: {
            return adapter.addMany(action.reports, state)
        }
        case report.SELECT: {
            return { ...state, selectedReportDefId: action.reportDefId }
        }
        default: {
            return state
        }
    }
}

export const getSelectedReportDefId = (state: State) => state.selectedReportDefId