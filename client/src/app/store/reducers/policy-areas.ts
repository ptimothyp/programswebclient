import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity'
import { PolicyArea } from '../models'
import * as policyArea from '../actions/policy-area'

export interface State extends EntityState<PolicyArea> {}

const getId = (pa: PolicyArea) => pa.id

export const adapter: EntityAdapter<PolicyArea> = createEntityAdapter<PolicyArea>({
    selectId: getId,
    sortComparer: false
})

export const initialState: State = adapter.getInitialState()

export function reducer(state = initialState, action: policyArea.Actions): State {
    switch (action.type) {
        case policyArea.LOAD_SUCCESS: {
            return adapter.addAll(action.policyAreas, state)
        }
        default: {
            return state
        }
    }
}

export const {
    selectAll: getAllPolicyAreas,
    selectEntities: getAllPolicyAreaEntities
} = adapter.getSelectors()
