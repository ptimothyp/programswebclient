import {
    ActionReducerMap,
    createSelector,
    createFeatureSelector,
    ActionReducer,
    MetaReducer,
} from '@ngrx/store'

import {
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    Params,
} from '@angular/router'

import * as moment from 'moment'
import * as fromLoginPage from '../../auth/store/reducers/login-page'
import * as fromAuth from '../../auth/store/reducers'
import * as fromRouter from '@ngrx/router-store'
import { AuthState } from '../../auth/store/reducers'
import { environment } from 'environments/environment'
import { storeFreeze } from 'ngrx-store-freeze'
import { localStorageSync } from 'ngrx-store-localstorage'
import * as fromElectorates from './electorates'
import * as fromPolicyAreas from './policy-areas'
import * as fromPrograms from './programs'
import * as fromStatistics from './statistics'
import * as fromReportDefinitions from './report-definitions'
import * as fromReports from './reports'
import { ListItem, ProgramReport, StatisticReport, ReportDefinition, Electorate } from '../models'

export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
    return localStorageSync({ keys: [{ 'auth': ['status'] }], rehydrate: true })(reducer)
}

export const metaReducers: MetaReducer<State>[] = !environment.production
    ? [localStorageSyncReducer, storeFreeze]
    : [localStorageSyncReducer]

export interface RouterStateUrl {
    url: string
    queryParams: Params
    params: Params
}

export interface State {
    electorates: fromElectorates.State,
    policyAreas: fromPolicyAreas.State,
    programs: fromPrograms.State,
    statistics: fromStatistics.State,
    reportDefinitions: fromReportDefinitions.State,
    reports: fromReports.State,
    routerReducer: fromRouter.RouterReducerState<RouterStateUrl>
}

export const reducers: ActionReducerMap<State> = {
    electorates: fromElectorates.reducer,
    policyAreas: fromPolicyAreas.reducer,
    programs: fromPrograms.reducer,
    statistics: fromStatistics.reducer,
    reportDefinitions: fromReportDefinitions.reducer,
    reports: fromReports.reducer,
    routerReducer: fromRouter.routerReducer
}

export const getPolicyAreasState = createFeatureSelector<fromPolicyAreas.State>('policyAreas')
export const getAllPolicyAreaEntities = createSelector(getPolicyAreasState, fromPolicyAreas.getAllPolicyAreaEntities)

export const getElectoratesState = createFeatureSelector<fromElectorates.State>('electorates')
export const getAllElectorates = createSelector(getElectoratesState, fromElectorates.getAllElectorates)
export const getSelectedElectorateName = createSelector(getElectoratesState, fromElectorates.getSelectedElectorateName)
export const getSelectedElectorate = createSelector(getElectoratesState, fromElectorates.getSelectedElectorate)
export const getFilteredElectorates = createSelector(getElectoratesState, fromElectorates.getFilteredElectorates)
export const getElectoratesList = createSelector(getAllElectorates, electorates => {
    const all = electorates.map((electorate: Electorate): ListItem =>
        ({
            id: electorate.name,
            name: `${electorate.name} (${electorate.state})`,
            category: `${electorate.member} (${electorate.party})`
        }))
    const national: ListItem = {
        id: '',
        name: 'National',
        category: ''
    }
    return [national, ...all]
})

export const getProgramsState = createFeatureSelector<fromPrograms.State>('programs')
export const getAllPrograms = createSelector(getProgramsState, fromPrograms.getAllPrograms)
export const getSelectedProgramId = createSelector(getProgramsState, fromPrograms.getSelectedProgramId)
export const getSelectedProgram = createSelector(getProgramsState, fromPrograms.getSelectedProgram)
export const getSelectedProgramPolicyArea = createSelector(getSelectedProgram, getAllPolicyAreaEntities, (program, policyAreas) =>
    program && policyAreas[program.policyAreaId])
export const getFilteredPrograms = createSelector(getProgramsState, fromPrograms.getFilteredPrograms)

export const getSelectedElectoratePrograms = createSelector(getAllPrograms, (programs) =>
    // TODO: filter programs not in electorate
    programs
)

export const getSelectedElectorateProgramsList
    = createSelector(getSelectedElectoratePrograms, getAllPolicyAreaEntities, (programs, policyAreas) =>
        programs.map((program): ListItem => {
            const policyArea = policyAreas[program.policyAreaId]
            return {
                id: program.id.toString(),
                name: program.name,
                category: policyArea && policyArea.name,
                colourClass: policyArea && policyArea.colourClass,
                iconClass: policyArea && policyArea.iconClass
            }
        })
    )

export const getStatisticsState = createFeatureSelector<fromStatistics.State>('statistics')
export const getStatisticEntities = createSelector(getStatisticsState, fromStatistics.getStatisticEntities)
export const getSelectedStatistic = createSelector(getStatisticsState, fromStatistics.getSelectedStatistic)

export const getSelectedElectorateStatistics = createSelector(getSelectedElectorate, getStatisticEntities, (electorate, statistics) =>
    electorate && electorate.statisticIds ? electorate.statisticIds.map(id => statistics[id]) : []
)

export const getReportDefinitionsState = createFeatureSelector<fromReportDefinitions.State>('reportDefinitions')
export const {
    selectEntities: getReportDefinitionEntities
} = fromReportDefinitions.adapter.getSelectors(getReportDefinitionsState)

export const getSelectedElectorateStatisticList
    = createSelector(getSelectedElectorateStatistics, getAllPolicyAreaEntities, (statistics, policyAreas) =>
        statistics.map((statistic): ListItem => {
            const policyArea = policyAreas[statistic.policyAreaId]
            return {
                id: statistic.id.toString(),
                name: statistic.name,
                category: policyArea && policyArea.name,
                colourClass: policyArea && policyArea.colourClass,
                iconClass: policyArea && policyArea.iconClass
            }
        })
)

export const getReportsState = createFeatureSelector<fromReports.State>('reports')
export const {
    selectAll: getReports
} = fromReports.adapter.getSelectors(getReportsState)
export const getSelectedReportDefId = createSelector(getReportsState, fromReports.getSelectedReportDefId)

// TODO: there must be a better way
const contextualiseReportDefinition = (definition: ReportDefinition, electorate: Electorate): ReportDefinition =>
    definition && {
        ...definition,
        schema: definition.schema && JSON.parse(JSON.stringify(definition.schema)
            .replace(/\[Electorate_Name\]/g, electorate.name)
            .replace(/\[State_Name\]/g, electorate.state))
    }

export const getSelectedProgramReports = createSelector(
    getSelectedProgramId, getSelectedElectorate, getReports, getReportDefinitionEntities,
    (programId, electorate, reports, definitions) => {
        if (!programId) {
            return []
        }
        if (!electorate) {
            return reports.filter((report: ProgramReport) => report.electorate === '' && report.programId === programId)
                .map(report =>
                    ({
                        ...report,
                        definition: definitions[report.definitionId]
                    }))
        }

        return reports.filter((report: ProgramReport) => report.electorate === electorate.name && report.programId === programId)
            .map(report =>
                ({
                    ...report,
                    definition: contextualiseReportDefinition(definitions[report.definitionId], electorate)
                }))
    }
)

export const getSelectedStatisticReports = createSelector(getSelectedStatistic, getSelectedElectorate, getReports, getReportDefinitionEntities,
    (statistic, electorate, reports, definitions) =>
        reports.filter((report: StatisticReport) => report.electorate === electorate.name && report.statisticId === statistic.id)
            .map(report =>
                ({
                    ...report,
                    definition: contextualiseReportDefinition(definitions[report.definitionId], electorate)
                }))
)

export const getRouterState = createFeatureSelector<
    fromRouter.RouterReducerState<RouterStateUrl>
    >('routerReducer')

export const selectAuthState = createFeatureSelector<fromAuth.AuthState>('auth')

export const selectAuthStatusState = createSelector(
    selectAuthState,
    (state: AuthState) => state.status
)

export const getLoggedIn = createSelector(
    selectAuthStatusState,
    (state) => {
        if (!state.auth) { return false }
        if (!state.auth.idToken) { return false }
        if (!state.auth.expiresIn) { return false }

        const isbefore = moment().isBefore(state.auth.expiresIn)

        return isbefore
    }
)

export const getUrl = (state: State) => state.routerReducer.state.url

export const getUser = createSelector(selectAuthState, (state: fromAuth.AuthState) => state.status.user)
export const getAuthToken = createSelector(selectAuthState, (state: fromAuth.AuthState) => state.status.auth.idToken)

export const redirectMagic = createSelector(
    getLoggedIn,
    getUrl,
    (loggedIn, url) =>
        ({
            loggedIn,
            url
        }))

export const selectLoginPageState = createSelector(
    selectAuthState,
    (state: AuthState) => state.loginPage
)
export const getLoginPageError = createSelector(
    selectLoginPageState,
    fromLoginPage.getError
)
export const getLoginPagePending = createSelector(
    selectLoginPageState,
    fromLoginPage.getPending
)

export class CustomSerializer
    implements fromRouter.RouterStateSerializer<RouterStateUrl> {
    serialize(routerState: RouterStateSnapshot): RouterStateUrl {
        const { url } = routerState
        const { queryParams } = routerState.root

        let state: ActivatedRouteSnapshot = routerState.root
        while (state.firstChild) {
            state = state.firstChild
        }
        const { params } = state

        return { url, queryParams, params }
    }
}