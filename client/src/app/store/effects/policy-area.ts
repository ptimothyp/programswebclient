import { Observable, of } from 'rxjs'
import { map, catchError, switchMap } from 'rxjs/operators'
import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'
import { Effect, Actions, ofType } from '@ngrx/effects'

import * as policyAreaActions from '../actions/policy-area'
import { DataService } from '../../services/data.service'

@Injectable()
export class PolicyAreaEffects {

    @Effect()
    loadPolicyAreas$: Observable<Action> = this.actions$.pipe(
        ofType(policyAreaActions.LOAD),
        switchMap(() =>
            this.dataService.getPolicyAreas()
                .pipe(
                    map(policyAreas => new policyAreaActions.LoadSuccess(policyAreas)),
                    catchError(error => of(new policyAreaActions.LoadFail(error)))
                )
        )
    )
    constructor(private actions$: Actions, private dataService: DataService) { }
}