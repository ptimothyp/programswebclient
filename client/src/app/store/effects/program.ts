import { Observable, of } from 'rxjs'
import { map, catchError, switchMap } from 'rxjs/operators'
import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'
import { Effect, ofType, Actions } from '@ngrx/effects'
import * as programActions from '../actions/program'
import * as reportActions from '../actions/report'
import { ReportDefinition, ProgramReport, DataType } from '../models'
import { DataService } from '../../services/data.service'
import { ReportDTO } from '../../services/models'

@Injectable()
export class ProgramEffects {
    @Effect()
    loadAllPrograms$: Observable<Action> = this.actions$.pipe(
        ofType(programActions.LOAD_ALL),
        switchMap(() =>
            this.dataService.getPrograms()
                .pipe(
                    map(programs => new programActions.LoadAllSuccess(programs)),
                    catchError(error =>
                        of(new programActions.LoadAllFail(error)))
                )
        )
    )

    @Effect()
    fetchProgramReports$: Observable<Action> = this.actions$.pipe(
        ofType(programActions.FETCH_REPORT_DATA),
        switchMap((action: programActions.FetchReportData) =>
            this.dataService.getProgramReports(action.programId, action.electorate)
                .pipe(
                    switchMap(reports => [
                        new reportActions.LoadReports(reports.map((report: ReportDTO): ProgramReport =>
                            ({
                                id: report.id,
                                data: report.data,
                                definitionId: report.definition.id,
                                electorate: action.electorate,
                                programId: action.programId
                            }))),
                        new reportActions.LoadDefinitions(reports.map((report: ReportDTO): ReportDefinition => {
                            const definition = report.definition
                            return {
                                id: definition.id,
                                name: definition.name,
                                format: DataType[definition.reportFormat],
                                schema: definition.schema,
                                dataDate: definition.dataDate,
                                notes: definition.notes
                            }
                        }))
                    ])
                )
        )
    )

    @Effect()
    fetchNationalProgramReports$: Observable<Action> = this.actions$.pipe(
        ofType(programActions.FETCH_NATIONAL_REPORT_DATA),
        switchMap((action: programActions.FetchReportData) =>
            this.dataService.getNationalProgramReports(action.programId)
                .pipe(
                    switchMap(reports => [
                        new reportActions.LoadReports(reports.map((report: ReportDTO): ProgramReport =>
                            ({
                                id: report.id,
                                data: report.data,
                                definitionId: report.definition.id,
                                electorate: '',
                                programId: action.programId
                            }))),
                        new reportActions.LoadDefinitions(reports.map((report: ReportDTO): ReportDefinition => {
                            const definition = report.definition
                            return {
                                id: definition.id,
                                name: definition.name,
                                format: DataType[definition.reportFormat],
                                schema: definition.schema,
                                dataDate: definition.dataDate,
                                notes: definition.notes
                            }
                        }))
                    ])
                )
        )
    )

    constructor(private actions$: Actions, private dataService: DataService) { }
}
