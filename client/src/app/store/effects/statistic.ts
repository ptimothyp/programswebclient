import { switchMap } from 'rxjs/operators'
import { Observable } from 'rxjs'
import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'
import { Effect, Actions, ofType } from '@ngrx/effects'

import * as statisticActions from '../actions/statistics'
import * as reportActions from '../actions/report'
import { DataService } from '../../services/data.service'
import { ReportDefinition, StatisticReport, DataType } from '../models'
import { ReportDTO } from '../../services/models'

@Injectable()
export class StatisticEffects {
    @Effect()
    fetchStatisticReports$: Observable<Action> = this.actions$.pipe(
        ofType(statisticActions.FETCH_REPORT_DATA),
        switchMap((action: statisticActions.FetchReportData) =>
            this.dataService.getStatisticReports(action.statisticId, action.electorate)
            .pipe(
                switchMap(reports => [
                    new reportActions.LoadReports(reports.map((report: ReportDTO): StatisticReport =>
                        ({
                            id: report.id,
                            data: report.data,
                            definitionId: report.definition.id,
                            electorate: action.electorate,
                            statisticId: action.statisticId
                        }))),
                    new reportActions.LoadDefinitions(reports.map((report: ReportDTO): ReportDefinition => {
                    const definition = report.definition
                        return {
                            id: definition.id,
                            name: definition.name,
                            format: DataType[definition.reportFormat],
                            schema: definition.schema,
                            dataDate: definition.dataDate,
                            notes: definition.notes
                        }
                    }))
                ])
            )
        )
    )

    constructor(private actions$: Actions, private dataService: DataService) { }
}
