import { map, catchError, switchMap, filter } from 'rxjs/operators'
import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'
import { Effect, Actions, ofType } from '@ngrx/effects'
import { Observable, of } from 'rxjs'

import * as electorateActions from '../actions/electorate'
import { DataService } from '../../services/data.service'
import { MapsService } from '../../services/maps.service'
import { Statistic, Program } from '../models'
import { ElectorateDTO, ProgramDTO } from '../../services/models'
import { StatisticDTO } from '../../services/models/statistics-dto'

@Injectable()
export class ElectorateEffects {
    @Effect()
    loadElectorates$: Observable<Action> = this.actions$.pipe(
        ofType(electorateActions.LOAD_ALL),
        switchMap(() =>
            this.dataService.getElectorates()
                .pipe(
                    map(electorates => new electorateActions.LoadAllSuccess(electorates)),
                    catchError(error => of(new electorateActions.LoadAllFail(error)))
                )
        )
    )

    @Effect()
    loadElectorate$: Observable<Action> = this.actions$.pipe(
        ofType(electorateActions.LOAD),
        switchMap((action: electorateActions.Load) =>
            this.dataService.getElectorateDetails(action.name)
                .pipe(
                    filter((electorate: ElectorateDTO) =>
                        electorate.statistics && electorate.statistics[0] != null
                    ),
                    switchMap((electorate: ElectorateDTO) => [
                        new electorateActions.LoadStatistics(action.name,
                            electorate.statistics.map((statistic: StatisticDTO): Statistic =>
                            ({
                                id: statistic.id,
                                name: statistic.name,
                                policyAreaId: statistic.policyArea && statistic.policyArea.id,
                                reportIds: statistic.reports.map(report => report.id)
                            }))),
                        new electorateActions.LoadPrograms(action.name,
                            electorate.programs.map((program: ProgramDTO): Program =>
                                ({
                                    id: program.id,
                                    name: program.name,
                                    agency: program.agency,
                                    reportIds: program.reports.map(report => report.id)
                                })))
                    ])
                )
        )
    )

    @Effect()
    loadMapData$: Observable<Action> = this.actions$.pipe(
        ofType(electorateActions.LOAD_MAP_DATA),
        map((action: electorateActions.LoadMapData) => action.electorateName),
        switchMap((electorateName) => this.mapService.getElectorateBoundary(electorateName)
            .pipe(
                map(data => new electorateActions.TransformMapData(data)),
                catchError(error => of(new electorateActions.LoadAllFail(error)))
            )
        )
    )

    @Effect()
    transformMapData$: Observable<Action> = this.actions$.pipe(
        ofType(electorateActions.TRANSFORM_MAP_DATA),
        map((action: electorateActions.TransformMapData) => action.rawMapData),
        switchMap((mapData) => mapData = this.mapService.getMapDataObject(mapData)
            .pipe(
                map(data => new electorateActions.TransformMapDataSuccess(data)),
                catchError(error => of(new electorateActions.LoadAllFail(error)))
            )
        )
    )

    constructor(private actions$: Actions, private dataService: DataService, private mapService: MapsService) { }
}