import { Action } from '@ngrx/store'
import { PolicyArea } from '../models'

export const LOAD = '[PolicyArea] Load'
export const LOAD_SUCCESS = '[PolicyArea] Load Success'
export const LOAD_FAIL = '[PolicyArea] Load Fail'

export class Load implements Action {
    readonly type = LOAD
    constructor() {}
}

export class LoadSuccess implements Action {
    readonly type = LOAD_SUCCESS
    constructor(public policyAreas: PolicyArea[]) {}
}

export class LoadFail implements Action {
    readonly type = LOAD_FAIL
    constructor(public error: any) {}
}

export type Actions =
Load
| LoadSuccess
| LoadFail