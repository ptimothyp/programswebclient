import { Action } from '@ngrx/store'

export const SELECT = '[Statistic] Select'

export const FETCH_REPORT_DATA = '[Statistic] Fetch Report Data'
export const FETCH_REPORT_DATA_FAIL = '[Statistic] Fetch Report Data Fail'

export class Select implements Action {
    readonly type = SELECT
    constructor(public statisticId: number) {}
}

export class FetchReportData implements Action {
    readonly type = FETCH_REPORT_DATA
    // TODO: change electorate to scope when introducing state and national
    constructor(public statisticId: number, public electorate: string) {}
}

export class FetchReportDataFail implements Action {
    readonly type = FETCH_REPORT_DATA_FAIL
    constructor(public error: any) {}
}

export type Actions =
Select
| FetchReportData
| FetchReportDataFail