import { Action } from '@ngrx/store'
import { Report, ReportDefinition } from '../models'

export const SELECT_STATISTIC_REPORT = '[Report] Select Statistic Report'

export const SELECT = '[Report] Select'

export const LOAD_DEFINITIONS = '[Report] Load Definitions'
export const LOAD_REPORTS = '[Report] Load Reports'

export class SelectStatReport implements Action {
    readonly type = SELECT_STATISTIC_REPORT
    constructor(public reportId: number) {}
}

export class Select implements Action {
    readonly type = SELECT
    constructor(public reportDefId: number) {}
}

export class LoadDefinitions implements Action {
    readonly type = LOAD_DEFINITIONS
    constructor(public definitions: ReportDefinition[]) {}
}

export class LoadReports implements Action {
    readonly type = LOAD_REPORTS
    constructor(public reports: Report[]) {}
}

export type Actions =
Select
| SelectStatReport
| LoadDefinitions
| LoadReports