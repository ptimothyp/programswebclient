import { Action } from '@ngrx/store'
import { Electorate, Program } from '../models'
import { Statistic } from '../models/statistic'

export const SELECT = '[Electorate] Select'
export const FILTER = '[Electorate] Filter'

export const LOAD_ALL = '[Electorate] Load All'
export const LOAD_ALL_SUCCESS = '[Electorate] Load All Success'
export const LOAD_ALL_FAIL = '[Electorate] Load All Fail'

export const LOAD = '[Electorate] Load'
export const LOAD_SUCCESS = '[Electorate] Load Success'
export const LOAD_FAIL = '[Electorate] Load Fail'

export const LOAD_STATISTICS = '[Electorate] Load statistics'
export const LOAD_PROGRAMS = '[Electorate] Load programs'

export const LOAD_MAP_DATA = '[Electorate] Load Map Data'
export const TRANSFORM_MAP_DATA = '[Electorate] Transform Map Data'
export const TRANSFORM_MAP_DATA_SUCCESS = '[Electorate] Transform Map Data Success'

export class Select implements Action {
    readonly type = SELECT
    constructor(public electorate: string) { }
}

export class Filter implements Action {
    readonly type = FILTER
    constructor(public filter: string) { }
}

export class LoadAll implements Action {
    readonly type = LOAD_ALL
    constructor() { }
}

export class LoadAllSuccess implements Action {
    readonly type = LOAD_ALL_SUCCESS
    constructor(public electorates: Electorate[]) { }
}

export class LoadAllFail implements Action {
    readonly type = LOAD_ALL_FAIL
    constructor(public error: any) { }
}

export class Load implements Action {
    readonly type = LOAD
    constructor(public name: string) { }
}

export class LoadSuccess implements Action {
    readonly type = LOAD_SUCCESS
    constructor(public electorates: any) { }
}

export class LoadFail implements Action {
    readonly type = LOAD_FAIL
    constructor(public error: any) { }
}

export class LoadStatistics implements Action {
    readonly type = LOAD_STATISTICS
    constructor(public electorate: string, public statistics: Statistic[]) {}
}

export class LoadPrograms implements Action {
    readonly type = LOAD_PROGRAMS
    constructor(public electorate: string, public programs: Program[]) {}
}

export class LoadMapData implements Action {
    readonly type = LOAD_MAP_DATA
    constructor(public electorateName: string) { }
}

export class TransformMapData implements Action {
    readonly type = TRANSFORM_MAP_DATA
    constructor(public rawMapData: any) { }
}

export class TransformMapDataSuccess implements Action {
    readonly type = TRANSFORM_MAP_DATA_SUCCESS
    constructor(public transformedMapData: any) { }
}

export type Actions =
    Select
    | LoadAll
    | LoadAllSuccess
    | LoadAllFail
    | Filter
    | LoadStatistics
    | LoadPrograms
    | LoadMapData
    | TransformMapData
    | TransformMapDataSuccess
    | Load
    | LoadFail
    | LoadSuccess
