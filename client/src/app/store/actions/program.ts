import { Action } from '@ngrx/store'
import { Program } from '../models'

export const SELECT = '[Program] Select'
export const FILTER = '[Program] Filter'

export const LOAD_ALL = '[Program] Load All'
export const LOAD_ALL_SUCCESS = '[Program] Load All Success'
export const LOAD_ALL_FAIL = '[Program] Load All Fail'

export const FETCH_REPORT_DATA = '[Program] Fetch Report Data'
export const FETCH_REPORT_DATA_FAIL = '[Program] Fetch Report Data Fail'

// TODO: consolidate these fetches with those above
export const FETCH_NATIONAL_REPORT_DATA = '[Program] Fetch National Report Data'
export const FETCH_NATIONAL_REPORT_DATA_FAIL = '[Program] Fetch National Report Data Fail'

export class Select implements Action {
    readonly type = SELECT
    constructor(public programId: number) {}
}

export class Filter implements Action {
    readonly type = FILTER
    constructor(public filter: string) { }
}

export class LoadAll implements Action {
    readonly type = LOAD_ALL
    constructor() {}
}

export class LoadAllSuccess implements Action {
    readonly type = LOAD_ALL_SUCCESS
    constructor(public programs: Program[]) {}
}

export class LoadAllFail implements Action {
    readonly type = LOAD_ALL_FAIL
    constructor(public error: any) {}
}

export class FetchReportData implements Action {
    readonly type = FETCH_REPORT_DATA
    // TODO: change electorate to scope when introducing state and national
    constructor(public programId: number, public electorate: string) {}
}

export class FetchReportDataFail implements Action {
    readonly type = FETCH_REPORT_DATA_FAIL
    constructor(public error: any) {}
}

export class FetchNationalReportData implements Action {
    readonly type = FETCH_NATIONAL_REPORT_DATA
    constructor(public programId: number) {}
}

export class FetchNationalReportDataFail implements Action {
    readonly type = FETCH_NATIONAL_REPORT_DATA_FAIL
    constructor(public error: any) {}
}

export type Actions =
Select
| Filter
| LoadAll
| LoadAllSuccess
| LoadAllFail
| FetchReportData
| FetchReportDataFail
| FetchNationalReportData
| FetchNationalReportDataFail