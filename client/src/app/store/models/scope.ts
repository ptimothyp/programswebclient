export enum Scope {
    Electorate,
    State,
    National
}