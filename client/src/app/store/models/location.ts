export interface Location {
    address: string
    coordinates: Coordinates
    electorate: string
}

export interface Coordinates {
    latitude: number
    longitude: number
}