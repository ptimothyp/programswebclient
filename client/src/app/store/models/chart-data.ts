export interface ChartData {
    name: string
    value: number
}

export interface MultiChartData {
    name: string
    series: ChartData[]
}