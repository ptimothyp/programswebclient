export interface Report {
    id: number
    definitionId: number
    data: any
    electorate: string
}

export interface ProgramReport extends Report {
    programId: number
}

export interface StatisticReport extends Report {
    statisticId: number
}