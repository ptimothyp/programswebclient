export interface ServiceData<T> {
    data: { [key: string]: T }
}