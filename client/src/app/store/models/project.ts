import { Location } from './location'

export interface Project {
    projectId: string
    name: string
    description: string
    status: string
    active: boolean
    caveats: string[]
    locations: Location[]
}