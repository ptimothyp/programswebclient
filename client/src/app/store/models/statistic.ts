export interface Statistic {
    id: number
    name: string
    policyAreaId: number
    reportIds: number[]
}