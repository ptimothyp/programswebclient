export interface TableColumn {
    label: string
    parentLabel?: string
    format?: 'string' | 'number' | 'date' | 'currency'
}

export interface UIColumn extends TableColumn {
    colSpan: number
}

export interface TableRow {
    label: string
    format?: 'total' | 'subtotal' | null
    columnData: (string|number)[]
}