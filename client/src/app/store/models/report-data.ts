import { TableRow, TableColumn } from './table-row'
import { MultiChartData } from './chart-data'

import { DataType } from './data-type'

// TODO: don't conflate component models with app models?
export class ReportData {
    title: string
    type: DataType
    data: any
    caveats: string[]
    electorate: string
    reportId: number
}

export class SummaryData extends ReportData {
    type = DataType.Summary
    data: string[]
}

export interface Table {
    schema: TableSchema,
    data: any[][]
}

export interface TableSchema {
    columns: TableColumn[]
    rows: TableRow[]
}

export class TableData extends ReportData {
    type = DataType.Table
    data: Table
}

export class BarChartData extends ReportData {
    type = DataType.BarChart
    data: MultiChartData[]
    xAxisLabel: string
}

export class LineChartData extends ReportData {
    type = DataType.LineChart
    data: MultiChartData[]
}