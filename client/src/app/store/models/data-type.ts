export enum DataType {
    Summary = 'Summary',
    Table = 'Table',
    BarChart = 'BarChart',
    LineChart = 'LineChart',
    PieChart = 'PieChart'
}