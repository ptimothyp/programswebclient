export interface PolicyArea {
    id: number
    name: string
    colourClass: string
    iconClass: string
}