import { MapData } from './map-data'

export interface Electorate {
    name: string
    state: string
    member: string
    party: string
    mapdata?: MapData
    population: number
    percentOfStatePopulation: number
    briefId: string
    statisticIds?: number[]
}
