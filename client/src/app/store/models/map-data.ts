export interface MapData {
    geoJson: any
    // centroid: LatLng
    bounds: any
}
export interface LatLng {
    lat: number
    long: number
}