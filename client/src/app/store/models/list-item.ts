export interface ListItem {
    id: string
    name: string
    category: string
    iconClass?: string
    colourClass?: string
}
