import { Project } from './project'

export interface Program {
    id: number
    name: string
    agency: string
    reportIds?: number[]

    description?: string
    status?: string
    active?: boolean
    caveats?: string[]
    projects?: Project[]
    policyAreaId?: number
}