import { DataType } from './data-type'

export interface ReportDefinition {
    id: number
    name: string
    format: DataType // TODO: rename DataType to ReportFormat?
    schema?: any
    dataDate: string
    notes: string
}