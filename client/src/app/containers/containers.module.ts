import { NgModule } from '@angular/core'
import { SharedModule } from '../shared/shared.module'
import { ComponentsModule } from '../components/components.module'
import { AppMaterialModule } from '../material/material.module'
import { BriefComponent } from './brief/brief.component'
import { MapComponent } from './map/map.component'
import { OverviewComponent } from './overview/overview.component'
import { ProgramsComponent } from './programs/programs.component'
import { StatisticsComponent } from './statistics/statistics.component'
import { CommitmentComponent } from './commitment/commitment.component'
import { MetricsComponent } from './metrics/metrics.component'

const CONTAINERS = [
    BriefComponent,
    MapComponent,
    OverviewComponent,
    ProgramsComponent,
    StatisticsComponent,
    CommitmentComponent,
    MetricsComponent
]

@NgModule({
    imports: [
        AppMaterialModule,
        SharedModule,
        ComponentsModule
    ],
    declarations: CONTAINERS,
    exports: CONTAINERS
})
export class ContainersModule { }
