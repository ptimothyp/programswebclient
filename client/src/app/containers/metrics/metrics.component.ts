import { Component, Input } from '@angular/core'
import { Store, select } from '@ngrx/store'
import { Observable } from 'rxjs'
import * as fromRoot from '../../store/reducers'
import * as programActions from '../../store/actions/program'
import * as electorateActions from '../../store/actions/electorate'
import * as reportActions from '../../store/actions/report'
import { ListItem, Program, Report, Electorate } from '../../store/models'

@Component({
  selector: 'app-metrics',
  templateUrl: './metrics.component.html',
  styleUrls: ['./metrics.component.scss']
})
export class MetricsComponent {
  @Input() programId: number

  selectedProgram$: Observable<Program>
  selectedProgramReports$: Observable<Report[]>
  selectedElectorate$: Observable<Electorate>

  electorateList$: Observable<ListItem[]>

  selectedReportDefId$: Observable<number>

  constructor(private store: Store<fromRoot.State>) {
    this.electorateList$ = store.pipe(select(fromRoot.getElectoratesList))
    this.selectedProgram$ = store.pipe(select(fromRoot.getSelectedProgram))
    this.selectedProgramReports$ = store.pipe(select(fromRoot.getSelectedProgramReports))
    this.selectedElectorate$ = store.pipe(select(fromRoot.getSelectedElectorate))
    this.selectedReportDefId$ = store.pipe(select(fromRoot.getSelectedReportDefId))

    this.store.dispatch(new electorateActions.Select(''))
  }

  handleSelectReport(item: ListItem) {
    if (item.id === '') {
      this.store.dispatch(new programActions.FetchNationalReportData(this.programId))
    }
    else {
      this.store.dispatch(new programActions.FetchReportData(this.programId, item.id))
    }
    this.store.dispatch(new electorateActions.Select(item.id))
  }

  handleSelectReportTab(reportdDefId: number) {
    this.store.dispatch(new reportActions.Select(reportdDefId))
  }
}
