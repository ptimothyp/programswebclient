import { Component, Input } from '@angular/core'
import { Store, select } from '@ngrx/store'
import { Observable } from 'rxjs'
import * as fromRoot from '../../store/reducers'
import * as statisticActions from '../../store/actions/statistics'
import * as reportActions from '../../store/actions/report'
import { ListItem, Statistic, Report } from '../../store/models'

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent {
  @Input() electorate: string

  definitions$: Observable<ListItem[]>
  selectedStatistic$: Observable<Statistic>
  selectedStatisticReports$: Observable<Report[]>

  selectedReportDefId$: Observable<number>

  constructor(private store: Store<fromRoot.State>) {
    this.definitions$ = store.pipe(select(fromRoot.getSelectedElectorateStatisticList))
    this.selectedStatistic$ = store.pipe(select(fromRoot.getSelectedStatistic))
    this.selectedStatisticReports$ = store.pipe(select(fromRoot.getSelectedStatisticReports))
    this.selectedReportDefId$ = store.pipe(select(fromRoot.getSelectedReportDefId))
  }

  handleSelectReport(item: ListItem) {
    const statisticId = parseInt(item.id, 10)
    this.store.dispatch(new statisticActions.FetchReportData(statisticId, this.electorate))
    this.store.dispatch(new statisticActions.Select(statisticId))
  }

  handleSelectReportTab(reportdDefId: number) {
    this.store.dispatch(new reportActions.Select(reportdDefId))
  }
}
