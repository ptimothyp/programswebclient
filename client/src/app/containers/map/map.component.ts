import { Component } from '@angular/core'
import { Observable } from 'rxjs'
import { Store, select } from '@ngrx/store'
import * as fromRoot from '../../store/reducers'
import { Electorate } from '../../store/models'

 @Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent {
  electorate$: Observable<Electorate>

  constructor(private store: Store<fromRoot.State>) {
    this.electorate$ = this.store.pipe(select(fromRoot.getSelectedElectorate))
  }
}
