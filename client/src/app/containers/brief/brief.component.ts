import { Component } from '@angular/core'
import { Electorate } from '../../store/models'
import { Store, select } from '@ngrx/store'
import { Observable } from 'rxjs'
import * as fromRoot from '../../store/reducers'

@Component({
  selector: 'app-brief',
  templateUrl: './brief.component.html',
  styleUrls: ['./brief.component.scss']
})
export class BriefComponent {
  electorate$: Observable<Electorate>

  constructor(private store: Store<fromRoot.State>) {
    this.electorate$ = this.store.pipe(select(fromRoot.getSelectedElectorate))
  }
}
