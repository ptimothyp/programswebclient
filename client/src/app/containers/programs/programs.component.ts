import { Component, Input } from '@angular/core'
import { Observable } from 'rxjs'
import { Store, select } from '@ngrx/store'
import * as fromRoot from '../../store/reducers'
import * as reportActions from '../../store/actions/report'
import * as programActions from '../../store/actions/program'
import { ListItem, Report, Program } from '../../store/models'

@Component({
  selector: 'app-programs',
  templateUrl: './programs.component.html',
  styleUrls: ['./programs.component.scss']
})
export class ProgramsComponent {
  @Input() electorate: string

  programsList$: Observable<ListItem[]>
  selectedProgram$: Observable<Program>
  selectedProgramReports$: Observable<Report[]>

  selectedReportDefId$: Observable<number>

  constructor(
    private store: Store<fromRoot.State>
  ) {
    this.programsList$ = store.pipe(select(fromRoot.getSelectedElectorateProgramsList))
    this.selectedProgram$ = store.pipe(select(fromRoot.getSelectedProgram))
    this.selectedProgramReports$ = store.pipe(select(fromRoot.getSelectedProgramReports))
    this.selectedReportDefId$ = store.pipe(select(fromRoot.getSelectedReportDefId))

    this.store.dispatch(new programActions.Select(null))
  }

  handleSelectReport(item: ListItem) {
    const programId = parseInt(item.id, 10)
    this.store.dispatch(new programActions.FetchReportData(programId, this.electorate))
    this.store.dispatch(new programActions.Select(programId))
  }

  handleSelectReportTab(reportdDefId: number) {
    this.store.dispatch(new reportActions.Select(reportdDefId))
  }

}
