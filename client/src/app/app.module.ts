import { BrowserModule } from '@angular/platform-browser'
import { APP_INITIALIZER, NgModule, ErrorHandler } from '@angular/core'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { StoreModule, Store, select } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'
import { StoreDevtoolsModule } from '@ngrx/store-devtools'
import { AppComponent } from './app.component'
import { CoreModule } from './core/core.module'
import { LayoutModule } from './layouts/layout.module'
import { SharedModule } from './shared/shared.module'
import { AppRoutingModule } from './app-routing.module'
import { dataServiceProvider } from './services/data.service.provider'
import { CustomErrorHandler } from './logging'
import { MapsService} from './services/maps.service'
import { reducers, metaReducers, CustomSerializer } from './store/reducers'
import * as fromRoot from './store/reducers'
import { ElectorateEffects, PolicyAreaEffects, ProgramEffects, StatisticEffects } from './store/effects'
import { AgmCoreModule } from '@agm/core'
import { AuthModule } from './auth/auth.module'
import { RouterStateSerializer, StoreRouterConnectingModule, } from '@ngrx/router-store'
import { environment } from 'environments/environment'
import { WINDOW_PROVIDERS } from './services/window.service'
import { JwtAuthInterceptor } from './auth/interceptors/jwtauth-interceptor'
import { StartAppInitialiser } from './store/actions/app.actions'
import { StartAutoTokenRefresh } from './auth/store/actions/auth'
import { GraphQLService } from './services/graphql.service'

export function initApplication(store: Store<fromRoot.State>): Function {
    return () => new Promise(resolve => {
      store.dispatch(new StartAppInitialiser())

      store.pipe(select(fromRoot.getLoggedIn)).subscribe(isLoggedIn => {
        if (isLoggedIn) {
          store.dispatch(new StartAutoTokenRefresh())
        }
      })

      resolve(true)
    })
  }

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        HttpClientModule,
        BrowserModule,
        BrowserAnimationsModule,
        CoreModule,
        LayoutModule,
        AuthModule.forRoot(),
        SharedModule.forRoot(),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyC0oSLzOmNWfY6mm0H0S3Qf8oGW8_yakuE',
            libraries: ['geometry']
          }),
        AppRoutingModule,
        StoreModule.forRoot(reducers, { metaReducers }),
        StoreRouterConnectingModule.forRoot({
          /*
            They stateKey defines the name of the state used by the router-store reducer.
            This matches the key defined in the map of reducers
          */
          stateKey: 'router',
        }),
         /**
         * Store devtools instrument the store retaining past versions of state
         * and recalculating new states. This enables powerful time-travel
         * debugging.
         *
         * To use the debugger, install the Redux Devtools extension for either
         * Chrome or Firefox
         *
         * See: https://github.com/zalmoxisus/redux-devtools-extension
         */
        !environment.production ? StoreDevtoolsModule.instrument({
            name: 'Subscription Manager DevTools',
            logOnly: environment.production,
        }) : [],
        EffectsModule.forRoot([ElectorateEffects, PolicyAreaEffects, ProgramEffects, StatisticEffects])
    ],
    providers: [
        {
          provide: ErrorHandler,
          useClass: CustomErrorHandler
        },
        WINDOW_PROVIDERS,
        {
          provide: APP_INITIALIZER,
          useFactory: initApplication,
          deps: [Store],
          multi: true
        },
        { provide: HTTP_INTERCEPTORS, useClass: JwtAuthInterceptor, multi: true },
        dataServiceProvider,
        GraphQLService,
        MapsService,
        { provide: RouterStateSerializer, useClass: CustomSerializer },
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
