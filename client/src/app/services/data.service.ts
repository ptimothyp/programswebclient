import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { PolicyArea, Electorate, Program, ServiceData } from '../store/models'
import { PolicyAreaDTO, ProgramDTO, ElectorateDTO, ReportDTO } from './models'
import { GraphQLService } from './graphql.service'

// TODO: complete and implement interface
export interface IDataService {
  getElectorateDetails(name: string)
  getElectorates(): Observable<Electorate[]>
  getProgramReports(id: number, electorate: string): Observable<ReportDTO[]>
  getNationalProgramReports(id: number): Observable<ReportDTO[]>
  getStatisticReports(id: number, electorate: string): Observable<ReportDTO[]>
  getPolicyAreas(): Observable<PolicyArea[]>
  getPrograms(): Observable<Program[]>
}

@Injectable()
export class DataService implements IDataService {

  constructor(private graphQL: GraphQLService) { }

  // TODO: change to observable pipe implementation after we upgrade to angular 5/6
  private unwrapServiceData<T>(serviceData: ServiceData<T>, property: string): T {
    return serviceData.data[property]
  }

  static readonly getElectoratesQuery = `{
    electorates {
      name
      state
      member
      party
      population
      percentOfStatePopulation
      briefId
    }
  }`
  getElectorates(): Observable<Electorate[]> {
    return this.graphQL.send<ElectorateDTO>(DataService.getElectoratesQuery)
      .pipe(map(response =>
        response.data['electorates'].map(electorate =>
          ({
            name: electorate.name,
            member: electorate.member,
            party: electorate.party,
            state: electorate.state,
            population: electorate.population,
            percentOfStatePopulation: electorate.percentOfStatePopulation,
            briefId: electorate.briefId
          })
        )
      ))
  }

  static readonly getElectorateDetailsQuery = `
  query ($name: String) {
    electorate(name: $name) {
      statistics {
        id
        name
        policyArea {
          id
        }
        reports(electorate: $name) {
          id
        }
      }
      programs {
        id
        name
        agency
        notes
        policyArea {
          id
        }
        reports(electorate: $name) {
          id
        }
      }
    }
  }`
  getElectorateDetails(name: string) {
    const variables: { [id: string]: any; } = {
      'name': name
    }
    return this.graphQL.send<ElectorateDTO>(DataService.getElectorateDetailsQuery, variables)
      .pipe(
        map(response => this.unwrapServiceData(response, 'electorate'))
      )
  }

  static readonly getProgramReportsQuery = `
  query($id: String, $electorate: String) {
    reportData
      (
        where: [
          {path: "Report.ProgramId", comparison: "equal", value: [$id]},
          {path: "Electorate.Name", comparison: "equal", value: [$electorate]}]
      )
      {
      id
      data
      definition {
        id
        name
        schema
        version
        notes
        reportFormat
        dataDate
        notes
      }
    }
  }`
  getProgramReports(id: number, electorate: string): Observable<ReportDTO[]> {
    const variables: { [id: string]: any; } = {
      'id': id.toString(),
      'electorate': electorate
    }

    return this.graphQL.send<ReportDTO>(DataService.getProgramReportsQuery, variables)
      .pipe(
        map(response => this.unwrapServiceData(response, 'reportData'))
      )
  }

  static readonly getNationalProgramReportsQuery = `
  query ($id: String) {
    reportData
    (
      where:
      [
        {path: "Report.ProgramId", comparison: "equal", value: [$id]},
        {path: "Electorate", comparison: "equal"}
      ]
    )
    {
      id
      data
      definition {
        id
        name
        schema
        version
        notes
        reportFormat
        dataDate
      }
    }
  }`
  getNationalProgramReports(id: number): Observable<ReportDTO[]> {
    const variables: { [id: string]: any; } = {
      'id': id.toString()
    }

    return this.graphQL.send<ReportDTO>(DataService.getNationalProgramReportsQuery, variables)
      .pipe(
        map(response => this.unwrapServiceData(response, 'reportData'))
      )
  }

  static readonly getStatisticReportsQuery = `
  query ($id: String, $electorate: String) {
    reportData
    (where:
      [
        {path: "Report.StatId", comparison: "equal", value: [$id]},
        {path: "Electorate.Name", comparison: "equal", value: [$electorate]}
      ]
    )
    {
      id
      data
      definition {
        id
        name
        schema
        version
        notes
        reportFormat
        dataDate
      }
    }
  }`
  getStatisticReports(id: number, electorate: string): Observable<ReportDTO[]> {
    const variables: { [id: string]: any; } = {
      'id': id.toString(),
      'electorate': electorate
    }

    return this.graphQL.send<ReportDTO>(DataService.getStatisticReportsQuery, variables)
      .pipe(
        map(response => this.unwrapServiceData(response, 'reportData'))
      )
  }

  static readonly getPolicyAreasQuery = `{
    policyAreas {
      id
      name
      icon
      colourClass
    }
  }`
  getPolicyAreas(): Observable<PolicyArea[]> {
    return this.graphQL.send<PolicyAreaDTO>(DataService.getPolicyAreasQuery)
      .pipe(
        map(response => response.data['policyAreas'].map((policyArea): PolicyArea =>
          ({
            id: policyArea.id,
            colourClass: policyArea.colourClass,
            name: policyArea.name,
            iconClass: policyArea.icon
          }))
        )
      )
  }

  static readonly getProgramsQuery = `{
    programs {
      id
      name
      agency
      notes
      policyArea {
        id
      }
    }
  }`
  getPrograms(): Observable<Program[]> {
    return this.graphQL.send<ProgramDTO>(DataService.getProgramsQuery)
      .pipe(
        map(response => response.data['programs'].map((program): Program =>
          ({
            id: program.id,
            name: program.name,
            agency: program.agency,
            policyAreaId: program.policyArea && program.policyArea.id
          })
        )
        )
      )
  }
}