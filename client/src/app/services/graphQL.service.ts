import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs'
import { environment } from 'environments/environment'
import { ServiceData } from '../store/models'

export interface GraphQLQuery {
    operationName: string
    variables: any
    query: string
}

@Injectable()
export class GraphQLService {

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        })
    }

    constructor(private http: HttpClient) { }

    private compressGraphQl(text: string) {
        const query = text.replace(/\s+/g, ' ')
        return query.replace(/\s*(\[|\]|\{|\}|\(|\)|:|\,)\s*/g, '$1')
    }

    public send<T>(query: string, variables: any = null): Observable<ServiceData<T[]>> {
        let variableString = ''
        if (variables) {
            variableString = JSON.stringify(variables)
        }

        const url = `${environment.dataServiceUrl}?query=${this.compressGraphQl(query)}&variables=${variableString}`
        return this.http.get<ServiceData<T[]>>(url, this.httpOptions)
    }
}
