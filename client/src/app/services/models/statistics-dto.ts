import { ReportDTO } from './report-dto'
import { PolicyAreaDTO } from './policy-area-dto'

export interface StatisticDTO {
    id: number
    name: string
    policyArea: PolicyAreaDTO
    reports: ReportDTO[]
}