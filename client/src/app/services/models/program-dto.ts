import { PolicyAreaDTO } from './policy-area-dto'
import { ReportDTO } from './report-dto'
export interface ProgramDTO {
    id: number
    name: string
    agency: string
    notes: string
    policyArea?: PolicyAreaDTO
    projects: any // TODO: define
    reports: ReportDTO[]
}