import { ReportDefinitionDTO } from './report-definition-dto'

export interface ReportDTO {
    id: number
    definition: ReportDefinitionDTO
    data: any
}