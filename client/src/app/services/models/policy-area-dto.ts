export interface PolicyAreaDTO {
    id: number
    name: string
    icon: string
    colourClass: string
}