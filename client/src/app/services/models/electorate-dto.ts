import { ProgramDTO } from './program-dto'
import { StatisticDTO } from './statistics-dto'
export interface ElectorateDTO {
    id: number
    name: string
    state: string
    party: string
    member: string
    population: number
    percentOfStatePopulation: number
    briefId: string
    statistics: StatisticDTO[]
    programs: ProgramDTO[]
}
