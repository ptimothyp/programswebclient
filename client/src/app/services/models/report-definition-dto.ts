export interface ReportDefinitionDTO {
    id: number
    name: string
    schema?: any
    version: number
    notes?: string
    reportFormat: string
    dataDate: string
}