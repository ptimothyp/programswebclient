import { DataService } from './data.service'
import { HttpClient } from '@angular/common/http'
import { IDataService } from './data.service'
import { FactoryProvider } from '@angular/core'
import { GraphQLService } from './graphql.service'

export const dataServiceFactory = (graphQL: GraphQLService): IDataService =>
    new DataService(graphQL)

export const dataServiceProvider: FactoryProvider = {
    provide: DataService,
    useFactory: dataServiceFactory,
    deps: [GraphQLService, HttpClient]
}