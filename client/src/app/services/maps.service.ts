import { Injectable } from '@angular/core'
import { Observable, from } from 'rxjs'
import { MapsAPILoader, LatLngBounds } from '@agm/core'
import { MapData } from '../store/models'
import { HttpClient } from '@angular/common/http'
import { environment } from 'environments/environment'

declare var google: any

export interface IMapsService {
  getMapDataObject(rawMapData: any): Observable<any> // TODO: try to use typed GeoJSON
  getElectorateBoundary(name: string): Observable<any> // TODO: try to use typed GeoJSON
}

const getLatLngBoundsFromBbox = (bbox: number[]): LatLngBounds => {
  if (!bbox || bbox.length < 4) {
    return null
  }
  const southWest = new google.maps.LatLng(bbox[1], bbox[0])
  const northEast = new google.maps.LatLng(bbox[3], bbox[2])
  return new google.maps.LatLngBounds(southWest, northEast)
}

@Injectable()
export class MapsService implements IMapsService {
  constructor(private http: HttpClient, private mapsApiLoader: MapsAPILoader) { }
  getMapDataObject(rawMapData: any): Observable<any> {
    return from(

      this.mapsApiLoader.load().then(() => {
        const validFeature = (rawMapData as any).features.find(feature => feature.geometry.coordinates && feature.geometry.coordinates.length >= 1)
        const allDecodedCoords: number[][][] = validFeature.geometry.coordinates.map(polygon =>
          polygon.map(coords =>
            google.maps.geometry.encoding.decodePath(coords)
              .map(pair => [pair.lng(), pair.lat()])
          )
        )

        const refactoredMapData: any = { ...validFeature, geometry: { type: 'MultiPolygon', coordinates: allDecodedCoords } }
        const bounds = refactoredMapData && getLatLngBoundsFromBbox(refactoredMapData.bbox)

        const mapsObj: MapData = {
          geoJson: refactoredMapData,
          bounds: bounds
        }

        return mapsObj
      })
    )
  }

  getElectorateBoundary(name: string) {
    return this.http.get(`${environment.googleMapsUrl}${encodeURI(name.toLowerCase())}`)
  }
}