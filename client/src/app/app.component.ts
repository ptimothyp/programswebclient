import { Component, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core'
import { Store } from '@ngrx/store'
import * as fromRoot from './store/reducers'
import * as electorate from './store/actions/electorate'
import * as policyArea from './store/actions/policy-area'
import * as program from './store/actions/program'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  title = 'Programs'

  constructor(private store: Store<fromRoot.State>) {
    this.store.dispatch(new electorate.LoadAll())
    this.store.dispatch(new policyArea.Load())
    this.store.dispatch(new program.LoadAll())
  }
}
