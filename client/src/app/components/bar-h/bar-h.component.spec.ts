import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { BarHChartComponent } from './bar-h.component'

describe('BarHChartComponent', () => {
  let component: BarHChartComponent
  let fixture: ComponentFixture<BarHChartComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarHChartComponent ]
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(BarHChartComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
