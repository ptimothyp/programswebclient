import { Component, Input } from '@angular/core'
import { MultiChartData } from '../../store/models'

@Component({
  selector: 'app-bar-h-chart',
  templateUrl: './bar-h.component.html',
  styleUrls: ['./bar-h.component.scss']
})
export class BarHChartComponent {

  @Input() chartData: MultiChartData[] // TODO: type the data
  @Input() xAxisLabel: string

  get view() {
      if (this.chartData.length === 1) {
        return [0, this.chartData.length * 155]
      }
      return [0, this.chartData.length * 60]

    }

  // options
  showXAxis = true
  showYAxis = true
  gradient = false
  showLegend = true
  showXAxisLabel = true
  showYAxisLabel = true
  yAxisLabel = ''

  colorScheme = {
    domain: ['#FC440F', '#1F01B9',  '#1EFFBC', '#B4E33D']
  }
}