import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core'
import { Report } from '../../store/models'

@Component({
  selector: 'app-data-card',
  templateUrl: './data-card.component.html',
  styleUrls: ['./data-card.component.scss']
})
export class DataCardComponent implements OnChanges {
  @Input() title: string
  @Input() reports: Report[]
  @Input() selectedReportDefId: number

  @Output() select = new EventEmitter<number>()

  _selectedReportDefId: number

  handleSelect(reportDefId: number) {
    this.select.emit(reportDefId)
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.reports && changes.reports.currentValue.length &&
        !changes.reports.currentValue.some(report => report.definitionId === this.selectedReportDefId)) {
      this._selectedReportDefId = changes.reports.currentValue[0].definitionId
    }
    else {
      this._selectedReportDefId = this.selectedReportDefId
    }
  }

  // TODO: resolve types with less magic string in template
}
