import { Component, Input } from '@angular/core'
import { OnInit } from '@angular/core'
import { LatLngLiteral, MapsAPILoader } from '@agm/core'

interface Marker {
  lat: number
  lng: number
  label?: string
  index: string
  draggable: boolean
  InfoWindowTextTitle?: string
  InfoWindowTextBody?: string
  InfoWindowIsOpen: boolean
  markerUrl?: string
}

@Component({
  selector: 'app-google-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss']
})

export class MapsComponent implements OnInit {
  @Input() bounds: any
  @Input() geoJson: any
  @Input() electorateName: string

  mapPoints: Marker[] = []
  kmlUrl: string

  constructor(private mapsApiLoader: MapsAPILoader) { }

  ngOnInit() {

    // Sample map points exist on for Aston only.
    // This will be removed once some real geo data is supplied.
    if (this.electorateName === 'Aston') {
      this.createMapPointsForAston()
      this.kmlUrl = ''
    }

    if (!this.electorateName) {
        // Assume this is National (no selected electorate)
        // Show a default map
        this.kmlUrl = 'https://raw.githubusercontent.com/JasonKitson/ElectorateMaps/master/RC1/BruceHighway.kmz'
        this.bounds = null
      }

  }

  getRandomInt(max): number {
    return Math.floor(Math.random() * Math.floor(max))
  }

  getPoint(lat: number, lng: number): Promise<any> {
    return this.mapsApiLoader.load().then(() =>
      new (window as any).google.maps.LatLng(lat, lng))
  }

  markerDragEnd(mrk: Marker, $event: MouseEvent): void {
    this.getPoint(($event as any).coords.lat, ($event as any).coords.lng).then(
      (pointResult) => {
        mrk.lat = pointResult.lat()
        mrk.lng = pointResult.lng()
      })
  }

  // TODO: get rid of this once we put real geo data in
  createMapPointsForAston(): void {
    let marker: Marker
    marker = {
      lat: -37.88988432844115, lng: 145.250012434082,
      draggable: false,
      InfoWindowIsOpen: false,
      InfoWindowTextTitle: 'Programme 1',
      InfoWindowTextBody: 'Mauris lobortis molestie erat, sodales faucibus nibh iaculis sit amet.',
      index: '1'
    }
    this.mapPoints.push(marker)
    marker = {
      lat: -37.860420535084,
      lng: 145.23996904785156,
      draggable: false,
      InfoWindowIsOpen: false,
      InfoWindowTextTitle: 'Programme 2',
      InfoWindowTextBody: 'Integer sem felis, consectetur in eros et, pellentesque vehicula enim.',
      index: '2'
    }
    this.mapPoints.push(marker)
    marker = {
      lat: -37.91846595723601,
      lng: 145.2680043066407,
      draggable: false,
      InfoWindowIsOpen: false,
      InfoWindowTextTitle: 'Programme 3',
      InfoWindowTextBody: 'Sed quis turpis dignissim, venenatis urna nec, tempus dolor.',
      index: '3'
    }
    this.mapPoints.push(marker)
  }

  generateMapPoints(borderOffSet: number, geoJson?): void {
    let random: number
    let childCtrlCounter = 0

    // reconstruct geoJson coordinates to suit gmaps Polygon object
    const arrRefectoredCoords: LatLngLiteral[] = geoJson && geoJson.geometry.coordinates.map(polygon => (polygon))[0][0].map(element =>
      ({
        lat: Number.parseFloat(element.toString().split(',')[1]),
        lng: Number.parseFloat(element.toString().split(',')[0])
      }))

    arrRefectoredCoords.forEach(coord => {
      random = this.getRandomInt(101)
      if (random % 40 === 0) {
        const m: Marker = {
          lat: coord.lat + borderOffSet,
          lng: coord.lng + borderOffSet,
          draggable: true,
          InfoWindowIsOpen: false,
          InfoWindowTextTitle: 'Random Marker ' + childCtrlCounter.toString(),
          InfoWindowTextBody: 'Some body text',
          index: childCtrlCounter.toString()
        }
        this.mapPoints.push(m)
        childCtrlCounter++
      }
    })
  }
}