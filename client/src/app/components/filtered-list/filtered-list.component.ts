import { Component, Input, Output, EventEmitter } from '@angular/core'
import { ListItem } from '../../store/models'

@Component({
  selector: 'app-filtered-list',
  templateUrl: './filtered-list.component.html',
  styleUrls: ['./filtered-list.component.scss']
})

export class FilteredListComponent {
  @Input() items: ListItem[]
  @Output() select = new EventEmitter<ListItem>()

  filter: string

  get filteredList() {
    if (!this.items) {
      return []
    }
    if (!this.filter) {
      return this.items
    }

    return this.items.filter(item =>
      this.match(item.name)
    )
  }

  handleClick(item: ListItem) {
    this.select.emit(item)
  }

  match(source: string): boolean {
    return this.filter && source && source.toLowerCase().indexOf(this.filter.toLowerCase()) > -1
  }
}
