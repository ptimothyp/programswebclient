import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { NoContentMessage } from './no-content-message.component'

describe('NoContentMessageComponent', () => {
  let component: NoContentMessageComponent
  let fixture: ComponentFixture<NoContentMessage>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoContentMessageComponent ]
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(NoContentMessageComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
