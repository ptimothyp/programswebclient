import { Component, Input, OnInit } from '@angular/core'
import { TableColumn, UIColumn, TableSchema } from '../../store/models'

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})

export class TableComponent implements OnInit {
  @Input() schema: TableSchema
  @Input() data: any[][]

  columnRows: UIColumn[][]

  ngOnInit() {
    this.columnRows = this.getUIColumns(this.schema.columns)
  }

  formatColumn(value: number | string, format: string): string {
    switch (format) {
      case 'number':
          return value && (value as number).toLocaleString('en-AU', { minimumFractionDigits: 0, maximumFractionDigits: 2})
      case 'percent':
          return value && (value as number).toLocaleString('en-AU', { minimumFractionDigits: 0, maximumFractionDigits: 1})
      default:
        return value.toString()
    }
  }

  getColumnClass(column: TableColumn) {
    switch (column.format) {
      case undefined: {
        return 'text-center'
      }
      case 'string': {
        return 'text-left'
      }
      default: {
        return 'text-right'
      }
    }
  }

  // TODO: make this recursive beyond two rows and decide on UIColumn model
  getUIColumns(columns: TableColumn[]): UIColumn[][] {
    if (!columns) {
      return []
    }
    const baseColumns = columns.map((column): UIColumn =>
      ({
        ...column,
        colSpan: 1
      }))

    const parents = columns.reduce((acc, column) => {
      if (column.parentLabel) {
        const parentCount = acc[column.parentLabel] || 0
        acc[column.parentLabel] = parentCount + 1
      }
      return acc
    }, {})

    const headerColumns = Object.keys(parents).map((key): UIColumn =>
      ({
        label: key,
        colSpan: parents[key]
      }))

    const uiColumns = [baseColumns]
    if (headerColumns && headerColumns.length) {
      uiColumns.unshift(headerColumns)
    }

    return uiColumns
  }
}
