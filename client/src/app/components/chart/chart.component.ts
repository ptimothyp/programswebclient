import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core'
import { TableRow } from '../../store/models'

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit, OnChanges {
  @Input() tableData: TableRow[]

  chartData: any[] = []

  showXAxis = true
  showYAxis = true
  gradient = false
  showLegend = true
  showXAxisLabel = false
  xAxisLabel = ''
  showYAxisLabel = false
  yAxisLabel = ''

  // pie
  showLabels = true
  explodeSlices = true
  doughnut = false

  pieColorScheme = {
      domain: [
          '#B9B9B9',
          '#A2EFFF',
          '#00B3E3',
          '#2ABFE7',
          '#61D095'
      ]
  }

  barColorScheme = {
      domain: [
          '#00B3E3',
          '#A2EFFF',
          '#B9B9B9'
      ]
  }

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.tableData && changes.tableData.currentValue) {
      this.chartData = this.buildChartData(changes.tableData.currentValue)
    }
  }

  buildChartData(tableData: TableRow[]) {
    return tableData.map((row) =>
        ({
            name: row.label,
            series: row.columnData
        }))
  }
}
