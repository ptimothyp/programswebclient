import { Component, Input } from '@angular/core'

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})

export class LineChartComponent {
  @Input() chartData: any[]

  // options
  showXAxis = true
  showYAxis = true
  gradient = false
  showLegend = true
  showXAxisLabel = true
  xAxisLabel = 'Budget Year'
  showYAxisLabel = true
  yAxisLabel = '$M'

  colorScheme = {
    domain: ['#FC440F', '#1EFFBC', '#7C9299', '#1F01B9', '#B4E33D']
  }

  // line, area
  autoScale = true
}
