import { NgModule } from '@angular/core'
import { SharedModule } from '../shared/shared.module'
import { AppMaterialModule } from '../material/material.module'
import { DataCardComponent } from './data-card/data-card.component'
import { TableComponent } from './table/table.component'
import { ChartComponent } from './chart/chart.component'
import { BarHChartComponent } from './bar-h/bar-h.component'
import { FilteredListComponent } from './filtered-list/filtered-list.component'
import { NoContentMessageComponent } from './no-content-message/no-content-message.component'
import { LineChartComponent } from './line-chart/line-chart.component'
import { MapsComponent } from './maps/maps.component'

const COMPONENTS = [
    DataCardComponent,
    TableComponent,
    ChartComponent,
    FilteredListComponent,
    NoContentMessageComponent,
    BarHChartComponent,
    LineChartComponent,
    MapsComponent
]

@NgModule({
    imports: [
        AppMaterialModule,
        SharedModule
    ],
    declarations: COMPONENTS,
    exports: COMPONENTS
})
export class ComponentsModule { }
