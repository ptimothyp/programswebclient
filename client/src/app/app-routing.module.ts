import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { MenuService } from './core/menu/menu.service'
import { FullLayoutComponent } from './layouts/layouts/full-layout/full-layout.component'
import { AuthGuard } from './auth/services/auth-guard.service'
import { SimpleLayoutComponent } from './layouts/layouts/simple-layout/simple-layout.component'

export const menu = [
    {
        name: 'Electorates',
        link: '/electorates',
        iconclass: 'ion-ios-location',
        order: 2
    },
    {
        name: 'Programs',
        link: '/programs',
        iconclass: 'ion-arrow-graph-up-right',
        order: 1
    },
    {
        name: 'Settings',
        link: '/settings',
        iconclass: 'ion-ios-settings',
        order: 3
    }
]

const routes: Routes = [{
    path: '',
    redirectTo: 'programs',
    pathMatch: 'full',

},
{
    path: '',
    component: FullLayoutComponent,
    data: {
        title: 'Programs'
    },
    canActivate: [AuthGuard],
    children: [
        {
            path: 'programs',
            // Don't change this to return the Module from a function - it will break AOT build
            loadChildren: './views/programs/programs.module#ProgramsModule'
        }
    ]
},
{
    path: '',
    component: FullLayoutComponent,
    data: {
        title: 'Electorates'
    },
    canActivate: [AuthGuard],
    children: [
        {
            path: 'electorates',
            // Don't change this to return the Module from a function - it will break AOT build
            loadChildren: './views/electorates/electorates.module#ElectoratesModule'
        }
    ]
},
{
    path: '',
    component: FullLayoutComponent,
    data: {
        title: 'Settings'
    },
    canActivate: [AuthGuard],
    children: [
        {
            path: 'settings',
            // Don't change this to return the Module from a function - it will break AOT build
            loadChildren: './views/settings/settings.module#SettingsModule'
        }
    ]
},
{
    path: 'pages',
    component: SimpleLayoutComponent,
    data: {
        title: 'Pages'
    },
    children: [
        {
            path: '',
            // Don't change this to return the Module from a function - it will break AOT build
            loadChildren: './views/pages#PagesModule'
        }
    ]
},
{ path: '**', redirectTo: 'pages/404' },
]

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        // enableTracing: true
    })], // TODO: turn off tracing.  currently on to diagnose problem in DEV
    exports: [RouterModule]
})
export class AppRoutingModule {
    constructor(private menuService: MenuService) {
        this.menuService.addMenu(menu)
    }
}
