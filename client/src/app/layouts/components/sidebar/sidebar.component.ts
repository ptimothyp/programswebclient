import { Component, OnInit, Injector, ViewEncapsulation } from '@angular/core'
import { Router, NavigationEnd } from '@angular/router'
import { filter } from 'rxjs/operators'

import { MenuService } from '../../../core/menu/menu.service'
import { SettingsService } from '../../../shared/settings/settings.service'

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class SidebarComponent implements OnInit {

    menu: any[]
    router: Router
    sidebarOffcanvasVisible: boolean

    constructor(private menuService: MenuService, public settings: SettingsService, private injector: Injector) {
        this.menu = this.menuService.getMenuSorted()
    }

    ngOnInit() {
        this.router = this.injector.get(Router)
        this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd)
            )
            .subscribe(() => {
                this.settings.app.sidebar.visible = false
            })
    }

    closeSidebar() {
        this.settings.app.sidebar.visible = false
    }

    handleSidebar(event) {
        const item = this.getItemElement(event)
        // check click is on a tag
        if (!item) {
            return
        }

        const lis = item.parentNode.parentNode // markup: ul > li > a
        // remove .active from childs
        lis.querySelectorAll('li').forEach((li) => {
            li.classList.remove('active')
        })
        // remove .active from siblings ()

        // Removed this while removing jquery. Didn't seem to be needed, at least while we only have a depth of 1
        // const liparent = item.parentNode[0];
        // lis.children.forEach(function(li) {
        //     if (li !== liparent) {
        //         li.classList.remove('active');
        //     }
        // });
        const next = item.nextElementSibling
        if (next && next.length && next[0].tagName === 'UL') {
            item.parentNode.toggleClass('active')
            event.preventDefault()
        }
    }

    // find the a element in click context
    // doesn't check deeply, asumens two levels only
    getItemElement(event) {
        const element = event.target
        const parent = element.parentNode
        if (element.tagName.toLowerCase() === 'a') {
            return element
        }
        if (parent.tagName.toLowerCase() === 'a') {
            return parent
        }
        if (parent.parentNode.tagName.toLowerCase() === 'a') {
            return parent.parentNode
        }
    }
}
