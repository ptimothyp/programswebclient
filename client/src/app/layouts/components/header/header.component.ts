import { Component, ViewEncapsulation } from '@angular/core'

import { SettingsService } from '../../../shared/settings/settings.service'

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss', './header.menu-links.scss'],
    encapsulation: ViewEncapsulation.None
})
export class HeaderComponent {

    sidebarVisible: true
    sidebarOffcanvasVisible: boolean

    constructor(public settings: SettingsService) { }

    toggleSidebarOffcanvasVisible() {
        this.settings.app.sidebar.offcanvasVisible = !this.settings.app.sidebar.offcanvasVisible
    }

    toggleSidebar(state = null) {
        //  state === true -> open
        //  state === false -> close
        //  state === null -> toggle
        this.settings.app.sidebar.visible = state !== null ? state : !this.settings.app.sidebar.visible
    }
}
