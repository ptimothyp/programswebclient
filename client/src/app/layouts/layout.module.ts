import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'

import { AppMaterialModule } from '../material/material.module'

import { SharedModule } from '../shared/shared.module'
import { HeaderComponent } from './components/header/header.component'
import { SidebarComponent } from './components/sidebar/sidebar.component'

import { SimpleLayoutComponent } from './layouts/simple-layout/simple-layout.component'
import { FullLayoutComponent } from './layouts/full-layout/full-layout.component'

@NgModule({
    imports: [
        AppMaterialModule,
        RouterModule,
        SharedModule
    ],
    declarations: [
        FullLayoutComponent,
        SidebarComponent,
        HeaderComponent,
        SimpleLayoutComponent
    ],
    exports: [
        FullLayoutComponent,
        SidebarComponent,
        HeaderComponent
    ]
})
export class LayoutModule { }
