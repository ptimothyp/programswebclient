import { Component } from '@angular/core'

@Component({
  selector: 'layout-simple',
  template: '<router-outlet></router-outlet>',
})
export class SimpleLayoutComponent { }
