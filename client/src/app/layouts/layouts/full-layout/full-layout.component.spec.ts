/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { DebugElement } from '@angular/core'

import { FullLayoutComponent } from './full-layout.component'
import { SettingsService } from '../../../shared/settings/settings.service'

describe('FullLayoutComponent', () => {
  it('should create', () => {
    const component = new FullLayoutComponent(new SettingsService())
    expect(component).toBeTruthy()
  })
})
