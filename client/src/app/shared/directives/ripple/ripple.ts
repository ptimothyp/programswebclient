const TRANSITION_END = 'transitionend webkitTransitionEnd'

/**
 * Ripple effect for common components
 */
export class RippleEffect {

    element
    rippleElement
    $rippleElement
    clickEv

    /**
     * @param [element] dom element
     */
    constructor(element) {
        this.element = element
        this.rippleElement = this.getElement()

        this.clickEv = this.detectClickEvent()

        this.element.addEventListener(this.clickEv, () => {
            // remove animation on click
            this.rippleElement.classList.remove('md-ripple-animate')
            // Set ripple size and position
            this.calcSizeAndPos()
            // start to animate
            this.rippleElement
                .classList.add('md-ripple-animate')
        })

        this.rippleElement.addEventListener(TRANSITION_END, () => {
            this.rippleElement
                .classList.remove('md-ripple-animate')
            // avoid weird affect when ripple is not active
            this.rippleElement.style.width = 0
            this.rippleElement.style.height = 0
        })
    }

    /**
     * Returns the elements used to generate the effect
     * If not exists, it is created by appending a new
     * dom element
     */
    getElement() {
        const dom = this.element
        let rippleElement = dom.querySelector('.md-ripple')

        if (rippleElement === null) {
            // Create ripple
            rippleElement = document.createElement('span')
            rippleElement.className = 'md-ripple'
            // Add ripple to element
            this.element.append(rippleElement)
        }
        return rippleElement
    }

    /**
     * Determines the better size for the ripple element
     * based on the element attached and calculates the
     * position be fully centered
     */
    calcSizeAndPos() {
        const bounds = this.element.getBoundingClientRect()
        const size = Math.max(bounds.width, bounds.height)
        this.rippleElement.style.width = size + 'px'
        this.rippleElement.style.height = size + 'px'
        // autocenter (requires css)
        this.rippleElement.style.marginTop = -(size / 2) + 'px'
        this.rippleElement.style.marginLeft = -(size / 2) + 'px'
    }

    detectClickEvent() {
        const isIOS = ((/iphone|ipad/gi).test(navigator.appVersion))
        return isIOS ? 'touchstart' : 'click'
    }

    destroy() {
        this.element.removeEventListener(this.clickEv, null)
        this.rippleElement.removeEventListener(TRANSITION_END, null)
    }
}
