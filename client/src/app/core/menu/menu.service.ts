import { Injectable } from '@angular/core'

@Injectable()
export class MenuService {

    menuItems: any[]

    constructor() {
        this.menuItems = []
    }

    addMenu(items: {
        name: string,
        link?: string,
        href?: string,
        imgpath?: string,
        order?: number,
        iconclass?: string,
        label?: any,
        subitems?: any[]
    }[]) {
        items.forEach((item) => {
            this.menuItems.push(item)
        })
    }

    getMenu() {
        return this.menuItems
    }

    getMenuSorted() {
        return this.menuItems.sort((a, b) =>
            a.order - b.order)
    }

}
