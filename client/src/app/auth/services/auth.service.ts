import { Injectable } from '@angular/core'
import { Store } from '@ngrx/store'
import { map, mergeMap } from 'rxjs/operators'
import { HttpClient } from '@angular/common/http'
import * as fromAuth from '../store/reducers'
import { AuthTokenRefresh, Logout } from '../store/actions/auth'
import { AuthResult, User } from '../store/models'
import { environment } from 'environments/environment'
import { Observable, of, timer } from 'rxjs'
import { JwtHelperService } from '@auth0/angular-jwt'

const CLAIM_EMAIL = 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress'
const CLAIM_NAME = 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name'

@Injectable()
export class AuthService {
  expiryAt$: any
  refreshSubscription: any
  jwtHelper: JwtHelperService

  constructor(private http: HttpClient, private store: Store<fromAuth.State>) {
    this.jwtHelper = new JwtHelperService()
  }

  logout() {
    this.store.dispatch(new Logout())
  }

  claims(token: string): Observable<User> {

    const decodedToken = this.jwtHelper.decodeToken(token)

    const email = decodedToken[CLAIM_EMAIL]
    const username = decodedToken[CLAIM_NAME]

    return of({
      title: username,
      background: 'red',
      displayType: 'circle',
      size: 35,
      email: email
    })
  }

  reissue(): Observable<AuthResult> {
    return this.http.get(`${environment.AUTH_BASE_PATH}/api/auth/refresh`, { responseType: 'text' })
      .pipe(
        map(
          token => {
            const expirationDate = this.getExpiryDateFrom(token)

            return {
              expiresIn: expirationDate,
              idToken: token
            }
          }))
  }

  public renewToken() {
    this.store.dispatch(new AuthTokenRefresh())
  }

  public scheduleRenewal() {
    const observableTimer = of(true).pipe(mergeMap(
      () =>
        timer(3600000)))

    // Once the delay time from above is
    // reached, dispatch a request for a token refresh and schedule another renewal
    this.refreshSubscription = observableTimer.subscribe(() => {
      this.renewToken()
      this.scheduleRenewal()
    })
  }

  public getExpiryDateFrom(token: string): Date {
    return this.jwtHelper.getTokenExpirationDate(token)
  }
}