import { Injectable } from '@angular/core'
import { CanActivate } from '@angular/router'
import { Store, select } from '@ngrx/store'

import { map, take } from 'rxjs/operators'
import * as Auth from '../store/actions/auth'
import * as fromRoot from '../../store/reducers'
import { Observable } from 'rxjs'

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private store: Store<fromRoot.State>) {}

  canActivate(): Observable<boolean> {
    return this.store.pipe(
      select(fromRoot.redirectMagic),
      map(authed => {
        if (!authed.loggedIn) {
          this.store.dispatch(new Auth.LoginRedirect(authed.url))
          return false
        }

        return true
      }),
      take(1)
    )
  }

}
