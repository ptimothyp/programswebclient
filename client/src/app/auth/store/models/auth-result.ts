export interface AuthResult {
    source?: string
    expiresIn: any
    idToken: string
  }