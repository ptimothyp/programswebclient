export interface User {
  title: string
  email: string
  background?: string
  displayType?: string
  size?: number
}
