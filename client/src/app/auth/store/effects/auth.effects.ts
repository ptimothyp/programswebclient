import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Actions, Effect, ofType } from '@ngrx/effects'
import { of } from 'rxjs'
import { catchError, filter, map, mergeMap, switchMap, take, tap } from 'rxjs/operators'
import * as Auth from '../actions/auth'
import * as fromRoot from '../../../store/reducers'
import { AuthService } from '../../services/auth.service'
import { Go } from '../../../store/actions'
import { select, Store, Action } from '@ngrx/store'
import {
  AuthActionTypes,
  AuthTokenRefresh,
  AuthTokenRefreshFailure,
  AuthTokenRefreshSuccess,
  AuthTokenSuccess, LoginFailure, LoginRedirect, LoginSuccess, StartAutoTokenRefresh
} from '../actions/auth'

@Injectable()
export class AuthEffects {

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private router: Router,
    private store: Store<any>
  ) {
  }

  @Effect()
  authSuccess$ = this.actions$.pipe(ofType<AuthTokenSuccess>(AuthActionTypes.AuthTokenSuccess))
    .pipe(
      map((action) => action.payload),
      switchMap((token) =>
        this.authService.claims(token.auth.idToken)
          .pipe(
            map(user => new LoginSuccess({ user })),
            catchError(error => of(new LoginFailure(error)))
          ))
    )

  @Effect({ dispatch: false })
  loginRedirect$ = this.actions$.pipe(
    ofType(AuthActionTypes.LoginRedirect, AuthActionTypes.Logout),
    tap((action: LoginRedirect) =>
      this.router.navigate(['/', 'login'], { queryParams: { o: action.payload } }))
  )

  @Effect()
  // handle location update
  locationUpdate$ = this.actions$
    .pipe(
    ofType('ROUTER_NAVIGATION'),
    filter((n: any) =>
      (n.payload.event.url.indexOf('token=') > 0)),
    mergeMap((action: any): Action[] => {

      // extract params from url
      const rS = action.payload.routerState
      const qP = rS.queryParams

      const expiryDate = this.authService.getExpiryDateFrom(qP.token)

      return [
        new AuthTokenSuccess({
          auth: {
            expiresIn: expiryDate,
            idToken: qP.token
          }
        }),
        new Go({ path: [rS.url.split('?')[0]] })
      ]
    }
    ))

  @Effect()
  loginReissue$ = this.actions$
    .pipe(
    ofType<AuthTokenRefresh>(AuthActionTypes.AuthTokenRefresh))
    .pipe(
      switchMap(
        () => this.authService.reissue()
          .pipe(
            map(authResult =>
              new AuthTokenRefreshSuccess(authResult)),
            catchError(error => of(new AuthTokenRefreshFailure(error)))
          )
      ))

  // TODO: this should dispath succes and failure
  @Effect({ dispatch: false })
  startAutoTokenRefresh$ = this.actions$
    .pipe(
    ofType<StartAutoTokenRefresh>(AuthActionTypes.StartAutoTokenRefresh))
    .pipe(
      map(_ => {
        this.authService.scheduleRenewal()
      })
    )

  @Effect()
  tokenExpired$ = this.actions$
    .pipe(
    ofType<AuthTokenRefreshFailure>(AuthActionTypes.AuthTokenRefreshFailure))
    .pipe(switchMap(_ =>
      this.store.pipe(select(fromRoot.redirectMagic),
        take(1),
        map(authed => new Auth.LoginRedirect(authed.url)))))
}
