import {AuthActions, AuthActionTypes} from '../actions/auth'
import {AuthResult, User} from '../models'

export interface State {
  user: User | null
  auth: AuthResult
}

export const initialState: State = {
  user: null,
  auth: null
}

export function reducer(state = initialState, action: AuthActions): State {
  switch (action.type) {
    case AuthActionTypes.LoginSuccess: {
      return {
        ...state,
        user: action.payload.user,
      }
    }

    case AuthActionTypes.AuthTokenSuccess: {

      return {
        ...state,
        auth: action.payload.auth,
      }
    }

    // TODO: check with Pete, what should we do when web api stops refreshing our token?  set it to null?
    case AuthActionTypes.AuthTokenRefreshFailure: {
      return {
        ...state,
        auth: null,
      }
    }

    case AuthActionTypes.AuthTokenRefreshSuccess: {
      return {
        ...state,
        auth: action.payload,
      }
    }

    case AuthActionTypes.Logout: {
      return initialState
    }

    default: {
      return state
    }
  }
}

export const getAuth = (state: State) => state.auth
export const getUser = (state: State) => state.user