import {
  ActionReducerMap,
} from '@ngrx/store'

import * as fromRootStore from '../../../store/reducers'
import * as fromAuth from './auth'
import * as fromLoginPage from './login-page'

export interface AuthState {
  status: fromAuth.State
  loginPage: fromLoginPage.State
}

export interface State extends fromRootStore.State {
  auth: AuthState
}

export const reducers: ActionReducerMap<AuthState> = {
  status: fromAuth.reducer,
  loginPage: fromLoginPage.reducer,
}