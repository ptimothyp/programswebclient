import { Injectable } from '@angular/core'
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http'
import { Observable } from 'rxjs'
import { environment } from 'environments/environment'
import { AuthState } from '../store/reducers'

@Injectable()
export class JwtAuthInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>,
    next: HttpHandler): Observable<HttpEvent<any>> {

    const auth: AuthState = JSON.parse(localStorage.getItem('auth'))
    const idToken = auth.status.auth.idToken

    const excludedUrls = [
      this.url_domain(environment.googleMapsUrl)
    ]

    if (excludedUrls.some(x => x === this.url_domain(req.url))) {
      return next.handle(req)
    }

    if (idToken) {
      const cloned = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + idToken)
      })

      return next.handle(cloned)
    }
    else {
      return next.handle(req)
    }
  }

  url_domain(data) {
    const a = document.createElement('a')
    a.href = data
    return a.hostname
  }

}
