import {Component, Inject, OnInit} from '@angular/core'
import {select, Store} from '@ngrx/store'
import * as fromRoot from '../../store/reducers'
import {WINDOW} from '../../services/window.service'
import {environment} from 'environments/environment'

@Component({
  selector: 'app-login-page',
  template: `
    <div class="app-announcement-wall federated-login-page">
      <div class="app-announcement-container">
        <h1 class="dark">Login</h1>
        <h3>Click <a mat-raised-button href="{{adfsurl}}">HERE</a> so we can verify who you are</h3>
        <p>If you are having problems authenticating please call our Customer Support team at 222-2222 I got an
          answering machine that can talk to you</p>
      </div>
    </div>
  `,
  styles: [],
})
export class LoginPageComponent implements OnInit {
  pending$ = this.store.pipe(select(fromRoot.getLoginPagePending))

  error$ = this.store.pipe(select(fromRoot.getLoginPageError))

  referrer: any
  adfsurl: string

  constructor(@Inject(WINDOW) private window: Window, private store: Store<fromRoot.State>) {this.referrer = `${window.location.origin}`

    // TODO:  tidy up for edge cases
    const returnUrl = this.window.location.search.split('=')

    if (returnUrl.length > 1) {
      this.adfsurl = `${environment.AUTH_BASE_PATH}/api/auth/login?returnUrl=${this.referrer}${returnUrl[1]}`
    }
    else {

      this.adfsurl = `${environment.AUTH_BASE_PATH}/api/auth/login?returnUrl=${this.referrer}`
    }
  }

  ngOnInit() {
    // TODO:  are we going to automatically redirect to ADFS
    // window.location.href = this.adfsurl;
  }

}
