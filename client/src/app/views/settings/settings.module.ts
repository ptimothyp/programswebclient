import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { AppMaterialModule } from '../../material/material.module'
import { SettingsRoutingModule } from './settings-routing.module'
import { IndexComponent } from './containers/index/index.component'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AppMaterialModule,
    SettingsRoutingModule
  ],
  declarations: [IndexComponent]
})
export class SettingsModule { }
