import { Component, OnInit, OnDestroy } from '@angular/core'
import { Store, select } from '@ngrx/store'
import { Subscription } from 'rxjs'
import { AuthService } from '../../../../auth/services/auth.service'
import * as fromRoot from '../../../../store/reducers'

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit, OnDestroy {

  token: string
  tokenSubscription$: Subscription

  constructor(private authService: AuthService, private store: Store<fromRoot.State>) { }

  ngOnInit() {
    this.tokenSubscription$ = this.store.pipe(select(fromRoot.getAuthToken)).subscribe(
      t => this.token = `Bearer ${t}`
    )
  }

  ngOnDestroy() {
    this.tokenSubscription$.unsubscribe()
  }

  refreshToken() {
    this.authService.renewToken()
  }

  logout() {
    this.authService.logout()
  }
}
