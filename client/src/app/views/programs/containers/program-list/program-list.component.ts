import { Component, OnInit } from '@angular/core'
import { Program } from '../../../../store/models'
import { Observable } from 'rxjs'
import { Store, select } from '@ngrx/store'
import * as program from '../../../../store/actions/program'
import * as fromRoot from '../../../../store/reducers'

@Component({
  selector: 'app-program-list',
  templateUrl: './program-list.component.html',
  styleUrls: ['./program-list.component.scss']
})
export class ProgramListComponent implements OnInit {

  programs$: Observable<Program[]>

  constructor(private store: Store<fromRoot.State>) {
    this.store.dispatch(new program.Filter(''))
    this.programs$ = store.pipe(select(fromRoot.getFilteredPrograms))
  }

  handleFilter(filter: any) {
    this.store.dispatch(new program.Filter(filter))
  }

  ngOnInit() {
  }
}
