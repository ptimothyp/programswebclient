import { Component, OnInit, OnDestroy } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Subscription, Observable } from 'rxjs'
import { Store, select } from '@ngrx/store'

import * as programActions from '../../../../store/actions/program'
import * as fromRoot from '../../../../store/reducers'
import { Program, PolicyArea } from '../../../../store/models'
@Component({
  selector: 'app-program-detail',
  templateUrl: './program-detail.component.html',
  styleUrls: ['./program-detail.component.scss']
})
export class ProgramDetailComponent implements OnInit, OnDestroy {
  id: number
  tab: string

  program$: Observable<Program>
  policyArea$: Observable<PolicyArea>

  private routeSubscription: Subscription

  handleSelectTab(tab: string) {
    this.router.navigate([`../${tab}`], { relativeTo: this.route })
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<fromRoot.State>
  ) {
    this.program$ = store.pipe(select(fromRoot.getSelectedProgram))
    this.policyArea$ = store.pipe(select(fromRoot.getSelectedProgramPolicyArea))
  }

  ngOnInit() {
    this.routeSubscription = this.route.params.subscribe(params => {
      this.id = parseInt(params['program'] as string, 10)
      this.tab = params['tab']

      this.store.dispatch(new programActions.Select(this.id))
      this.store.dispatch(new programActions.FetchNationalReportData(this.id))
    })
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe()
  }
}
