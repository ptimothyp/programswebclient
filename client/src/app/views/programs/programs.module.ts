import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { AppMaterialModule } from '../../material/material.module'
import { SharedModule } from '../../shared/shared.module'
import { ContainersModule } from '../../containers/containers.module'
import { ComponentsModule } from '../../components/components.module'

import { ProgramsRoutingModule } from './programs-routing.module'
import { ProgramListComponent } from './containers/program-list/program-list.component'
import { ProgramDetailComponent } from './containers/program-detail/program-detail.component'

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AppMaterialModule,
    ContainersModule,
    ComponentsModule,
    ProgramsRoutingModule
  ],
  declarations: [ ProgramListComponent, ProgramDetailComponent]
})
export class ProgramsModule { }
