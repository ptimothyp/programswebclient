import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { MapsComponent } from '../../components/maps/maps.component'
import { ProgramListComponent } from './containers/program-list/program-list.component'
import { ProgramDetailComponent } from './containers/program-detail/program-detail.component'

const routes: Routes = [
  {
    path: '',
    component: ProgramListComponent
  },
  {
    path: ':program/map',
    component: ProgramDetailComponent,
    children: [
      { path: '', component: MapsComponent }
    ]
  },
  {
    path: ':program',
    redirectTo: ':program/overview',
    pathMatch: 'full'
  },
  {
    path: ':program/:tab',
    component: ProgramDetailComponent
  },
  { path: '**', redirectTo: '' }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgramsRoutingModule { }
