import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { AppMaterialModule } from '../../material/material.module'
import { SharedModule } from '../../shared/shared.module'
import { ContainersModule } from '../../containers/containers.module'
import { ComponentsModule } from '../../components/components.module'

import { ElectoratesRoutingModule } from './electorates-routing.module'
import { ElectorateDetailComponent } from './containers/electorate-detail/electorate-detail.component'
import { ElectorateListComponent } from './containers/electorate-list/electorate-list.component'

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AppMaterialModule,
    ComponentsModule,
    ContainersModule,
    ElectoratesRoutingModule
  ],
  declarations: [ElectorateDetailComponent, ElectorateListComponent]
})
export class ElectoratesModule { }
