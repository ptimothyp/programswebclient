import { Component, OnInit, OnDestroy } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Subscription, Observable } from 'rxjs'
import { Store, select } from '@ngrx/store'

import * as electorateActions from '../../../../store/actions/electorate'
import * as fromRoot from '../../../../store/reducers'
import { Electorate } from '../../../../store/models'

@Component({
  selector: 'app-electorate-detail',
  templateUrl: './electorate-detail.component.html',
  styleUrls: ['./electorate-detail.component.scss']
})
export class ElectorateDetailComponent implements OnInit, OnDestroy {
  electorateName: string
  tab: string

  electorate$: Observable<Electorate>

  private routeSubscription: Subscription

  handleSelectTab(tab: string) {
      this.router.navigate([`../${tab}`], { relativeTo: this.route })
  }

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private store: Store<fromRoot.State>
  ) {
      this.electorate$ = store.pipe(select(fromRoot.getSelectedElectorate))
  }

  ngOnInit() {
      this.routeSubscription = this.route.params.subscribe(params => {
          this.electorateName = params['electorate']
          this.tab = params['tab']

          this.store.dispatch(new electorateActions.Load(this.electorateName))
          this.store.dispatch(new electorateActions.Select(this.electorateName))
          // this.store.dispatch(new reportActions.LoadElectorateReports(this.electorateName))
           this.store.dispatch(new electorateActions.LoadMapData(this.electorateName))
      })
  }

  ngOnDestroy() {
      this.routeSubscription.unsubscribe()
  }
}
