import { Component, OnInit } from '@angular/core'
import { Electorate } from '../../../../store/models'
import { Observable } from 'rxjs'
import { Store, select } from '@ngrx/store'
import * as electorate from '../../../../store/actions/electorate'
import * as fromRoot from '../../../../store/reducers'

@Component({
  selector: 'app-electorate-list',
  templateUrl: './electorate-list.component.html',
  styleUrls: ['./electorate-list.component.scss']
})
export class ElectorateListComponent implements OnInit {

  electorates$: Observable<Electorate[]>

  constructor(private store: Store<fromRoot.State>) {
    this.store.dispatch(new electorate.Filter(''))
    this.electorates$ = store.pipe(select(fromRoot.getFilteredElectorates))
  }

  handleFilter(filter: any) {
    this.store.dispatch(new electorate.Filter(filter))
  }

  ngOnInit() {
  }
}
