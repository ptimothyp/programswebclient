import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { MapComponent } from '../../containers/map/map.component'
import { ElectorateListComponent } from './containers/electorate-list/electorate-list.component'
import { ElectorateDetailComponent } from './containers/electorate-detail/electorate-detail.component'

const routes: Routes = [
  {
    path: '',
    component: ElectorateListComponent
  },
  {
    path: ':electorate/map',
    component: ElectorateDetailComponent,
    children: [
      { path: '', component: MapComponent }
    ]
  },
  {
    path: ':electorate',
    redirectTo: ':electorate/overview',
    pathMatch: 'full'
  },
  {
    path: ':electorate/:tab',
    component: ElectorateDetailComponent
  },
  { path: '**', redirectTo: '' }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ElectoratesRoutingModule { }
