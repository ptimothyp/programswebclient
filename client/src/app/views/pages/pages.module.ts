import { NgModule } from '@angular/core'
import { ReactiveFormsModule } from '@angular/forms'

import { P404Component } from './404.component'
import { P500Component } from './500.component'
import { PagesRoutingModule } from './pages-routing.module'

@NgModule({
  imports: [
    ReactiveFormsModule,
    PagesRoutingModule
   ],
  declarations: [
    P404Component,
    P500Component
  ]
})
export class PagesModule { }
