import { ErrorHandler } from '@angular/core'

export class CustomErrorHandler implements ErrorHandler {
  handleError(error) {
    // TODO: log error
    // IMPORTANT: Rethrow the error otherwise it gets swallowed
    throw error
  }
}