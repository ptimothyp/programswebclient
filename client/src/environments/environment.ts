export const environment = {
  production: false,
  AUTH_BASE_PATH: '//localhost:52619',
  dataServiceUrl: '//localhost:52619/graphql/',
  googleMapsUrl: '//ausvotes.withgoogle.com/electorates/12?ids='
}
