export const environment = {
  production: true,
  AUTH_BASE_PATH: '#{AuthBasePath}',
  dataServiceUrl: '#{DataServiceUrl}',
  googleMapsUrl: '#{GoogleMapsUrl}'
}
