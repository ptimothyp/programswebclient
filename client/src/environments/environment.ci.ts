export const environment = {
  production: false,
  AUTH_BASE_PATH: '//programs.cloud9.cabnet',
  dataServiceUrl: '//programs.cloud9.cabnet/graphql',
  googleMapsUrl: '//ausvotes.withgoogle.com/electorates/12?ids='
}
