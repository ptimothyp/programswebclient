# Getting started

## Install Dev stack/tools

### Node

[node 8.11.3](https://nodejs.org/dist/v8.11.3/node-v8.11.3-x64.msi)

### NPM

`npm i -g npm@6.2.0`

### Typescript linting

```bash
npm install -g tslint typescript
npm install -g rxjs-tslint
```

Install the TSLint VS Code extension

### Package management

Install npm-check for better package management

`npm i -g npm-check`

Running `npm-check` will help find unused pacakges, while `npm-check -u` runs an interactive upgrade that is pretty useful.

## Install npm packages

Work out of the folder that contains package.json (/client),

`npm install`

## Run app

`npm run start-local`

## Angular CLI cheatsheet

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0.

## Development server

Run `npm run start-local` for a dev server. Navigate to `https://localhost:4200/`.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
